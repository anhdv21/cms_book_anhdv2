<?php

namespace App\DataTables;

use App\Model\Book;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class BooksDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return $this->datatables
        ->eloquent($this->query())
        ->make(true);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Book $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $books = Book::all();
        return $this->applyScopes($books);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns([
            'id',
            'bookname',
            'author',
            'category_name',
            'review_content',
            'nguoi_dang_bai',
        ])
        ->parameters([
            'dom' => 'Bfrtip',
            'buttons' => ['csv', 'excel', 'pdf', 'print', 'reset', 'reload'],
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
            Column::make('id'),
            Column::make('bookname'),
            Column::make('author'),
            Column::make('category_name'),
            Column::make('review_content'),
            Column::make('nguoi_dang_bai'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Books_' . date('YmdHis');
    }
}
