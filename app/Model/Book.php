<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use willvincent\Rateable\Rateable;

class Book extends Model
{
    use Rateable;
    protected $table = 'books';
    public $timestamp = false;

    protected $fillable = [
        'id','bookname', 'author', 'file_path', 'review_content', 'categoryid'
    ];

    public function category()
    {
        return $this->belongsTo('App\Model\Category');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\Account');
    }
}
