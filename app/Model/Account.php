<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'users';
    public $timestamps = false;

    protected $fillable = [
        'username', 'name', 'email', 'password', 'level'
    ];

    public function books()
    {
        return $this->hasMany('App\Model\Book','id_nguoi_dang');
    }
}
