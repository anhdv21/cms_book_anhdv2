<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Authors extends Model
{
    protected $table = 'author';
    public $timestamps = false;

    public function posts(){
        return $this->hasMany('App\Posts','author_id');
    }
}
