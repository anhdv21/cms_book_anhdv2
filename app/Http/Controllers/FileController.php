<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileController extends Controller
{
    public function index()
    {
        return view('demo_upload');
    }

    public function upload(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:jpg,jpeg',
        ]);
        $fileName = $request->file->getClientOriginalName();  
   
        $request->file->move('upload', $fileName);
   
        return redirect()->route('upload-form')
            ->with('success','You have successfully upload file '.$fileName);
    }
}
