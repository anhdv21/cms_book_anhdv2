<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use DataTables;
use Yajra\DataTables\Facades\DataTables as FacadesDataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*if ($request->ajax()) {
            $data = User::all();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $id = $row->id; 
                           $btn = '<a href="'.route('account-description',['id'=>$id]).'"  class="edit btn btn-primary btn-sm">View</a>';
                           $btn = $btn.'<a href="'.route('account-description',['id'=>$id]).'" class="edit btn btn-danger btn-sm" ">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('users');*/
        $users = User::all();
        return view('users',['users'=>$users]);
    }

    public function getUser()
    {
        $users = User::select(['id','name','email']);
        return FacadesDatatables::of($users)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $id = $row->id; 
                           $btn = '<a href="'.route('account-description',['id'=>$id]).'"  class="edit btn btn-primary btn-sm">View</a>';
                           $btn = $btn.'<a href="'.route('account-description',['id'=>$id]).'" class="edit btn btn-danger btn-sm" ">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
