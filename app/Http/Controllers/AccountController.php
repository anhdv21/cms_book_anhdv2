<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Account;
use App\Http\Requests\AccountRequest;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\BookController;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = Account::paginate(2);
        return view('account/index',['accounts' => $accounts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('account/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccountRequest $request)
    {
        $input = $request->validated();
        $data = new Account();
        $data->username = $input['username'];
        $data->name = $input['name'];
        $data->email = $input['email'];
        $data->password = Hash::make($input['password']);
        if( $input['role'] == 1)
        {
            $data->level = 1;
        }
        else
        {
            $data->level = 2;
        }
        $data->save();
        return redirect()->route('account-show')->with('success', 'Add Successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account = Account::find($id);
        return view('account/view',['account'=>$account]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AccountRequest $request, $id)
    {
        $data = Account::where('id',$id)->first();
        $input = $request->validated();

        $data->username = $input['username'];
        $data->name = $input['name'];
        $data->email = $input['email'];
        $data->password = Hash::make($input['password']);
        if( $input['role'] == 1)
        {
            $data->level = 1;
        }
        else
        {
            $data->level = 2;
        }

        /*Thay doi thong tin nguoi dang trong book*/
        $books = $data->books;
        foreach( $books as $book )
        {
            $book->nguoi_dang_bai = $data->username;
            $book->save();
        }
        
        $data->save();

        return redirect()->route('account-description',$id)->with('success', 'Update Successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $data = Account::where('id',$id)->first();
        $books = $data->books;

        /*Xoa bo sach*/
        foreach($books as $book)
        {
            app('App\Http\Controllers\BookController')->destroy($book->id);
        }

        /*Xoa bo user*/
        $destroy = Account::destroy($id);
        return redirect()->route('account-show');
    }
}
