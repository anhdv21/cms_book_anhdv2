<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Category;
use App\Http\Requests\AddCategoryRequest;
use App\Http\Requests\CategoryRequest;
use App\Model\Book;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::all();
        $childCategories = Category::whereNotNull('parentId')->get(); /*Tra ve thu muc con de doi chieu*/
        return view('category/index', ['categories' => $categories, 'childCategories' => $childCategories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('parentId', null)->get(); /*Tra ve Thu Muc Cha De Select*/
        return view('category/add', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $input = $request->validated();
        $data = new Category();

        /*Neu Thu Muc Nhap Vao La Thu Muc Cha*/
        if ($input['parentcategoryid'] == 'none') {
            $data->categoryname = $input['categoryname'];
            $data->parentId = null;
            $data->save();
            return redirect()->route('category-show')->with('success', 'Add Successful');
        }
        /*Neu Thu Muc Nhap Vao La Thu Muc Con*/ else {
            $parentcategoryid = $input['parentcategoryid'];
            $data->categoryname = $input['categoryname'];
            $data->parentId = $parentcategoryid;
            $data->save();
            return redirect()->route('category-show')->with('success', 'Add Successful');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::where('id', $id)->first();
        $books = Book::where('category_id', $id)->get(); /*Tra ve Book Cua Thu Muc*/
        $categories = Category::where('parentId', null)->get();
        return view('category/view', ['category' => $category, 'categories' => $categories, 'books' => $books]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $input = $request->validated();
        $data = Category::where('id', $id)->first();


        /*Select La thu muc cha*/
        if ($input['parentcategoryid'] == 'none') 
        {   /*Xu ly Thu Muc Cha Hien Tai */
            $parentCategory = Category::where('id', $data->parentId)->first();
            $parentCategory->decrement('number_of_book', $data->number_of_book);
            if( $parentCategory->number_of_book == 0)
            {
                $parentCategory->has_books = 0;
            }
            $parentCategory->save();

            $data->categoryname = $input['categoryname'];
            $data->parentId = null;
            $data->save();

            /*Select La thu muc con */
        } else 
        {   
            if($data->parentId != $input['parentcategoryid']) /*Thu Muc Update Co Parent Khac Thu Muc Ban Dau */
            {
            if($data->parentId != null) /* La Thu Muc Con */
            {
            /* Xu Ly Thu Muc Cha Hien Tai */
            $parentCategory = Category::where('id', $data->parentId)->first();
            $parentCategory->decrement('number_of_book', $data->number_of_book);
            if( $parentCategory->number_of_book == 0)
            {
                $parentCategory->has_books = 0;
            }
            $parentCategory->save();

            /* Xu Ly Thu Muc Cha Chua Thu Muc Cap Nhat */
            $newParentCategory = Category::where('id', $input['parentcategoryid'])->first();
            $newParentCategory->increment('number_of_book', $data->number_of_book);
            if( $newParentCategory->number_of_book > 0)
            {
                $newParentCategory->has_books = 1;
            }
            $newParentCategory->save();
            }
            else /* La Thu Muc Cha */
            {
            /* Xu Ly Thu Muc Cha Chua Thu Muc Cap Nhat */
            $newParentCategory = Category::where('id', $input['parentcategoryid'])->first();
            $newParentCategory->increment('number_of_book', $data->number_of_book);
            if( $newParentCategory->number_of_book > 0)
            {
                $newParentCategory->has_books = 1;
            }
            $newParentCategory->save();
            }
            }

            $data->categoryname = $input['categoryname'];
            $data->parentId = $input['parentcategoryid'];
            $data->save();
        }

        /* Luu ten thu muc moi trong book */
        $books = $data->books;
        foreach ($books as $book) {
            $book->category_name = $data->categoryname;
            $book->save();
        }

        return redirect()->route('category-description', $id)->with('success', 'Update Successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Category::where('id', $id)->first();
        if ($data->has_books == 1) { /*Neu Thu Muc Can Xoa Co Sach*/
            return redirect()->route('category-show')->with('hasBookError', 'Error: The category has books.');
        } else { /*Neu Thu Muc Can Xoa Khong Co Sach*/
            if (!is_null($data->parentId)) { /*Neu Thu Muc La Thu Muc Con*/
                Category::findOrFail($id)->delete();
                return redirect()->route('category-show')->with('deleteSuccess', 'You have deleted the category');
            } else { /*Neu La Thu Muc Cha */
                $temp = Category::where('parentId', $id)->first();
                if (!is_null($temp)) { /*Neu Co Thu Muc Con Phu Thuoc*/
                    return redirect()->route('category-show')->with('error', 'Error: The category has sub-categories.');
                } else { /* Neu Khong Co Thu Muc Con Phu Thuoc*/
                    Category::findOrFail($id)->delete();
                    return redirect()->route('category-show')->with('deleteSuccess', 'You have deleted the category');
                }
            }
        }
    }
}
