<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Book;
use App\Http\Requests\BookRequest;
use App\Model\Category;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use willvincent\Rateable\Rateable;
class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $books = Book::orderBy('nguoi_dang_bai', 'asc')->get(); /*Sap Xep Sach Theo Ten*/
        $user = Auth::user(); /*Tra ve user hien tai dang login*/
        return view('book/index', ['books' => $books, 'user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::whereNotNull('parentId')->get(); /*Tra ve thu muc con de select*/
        return view('book/add', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        $input = $request->validated();
        $book = new Book();

        $book->bookname = $input['bookname'];
        $book->author = $input['author'];
        $book->review_content = $input['review'];
        $book->category_id = $input['categoryid'];

        /*Thu muc con chua book duoc add*/
        $category = Category::where('id', $input['categoryid'])->first();
        $category->has_books = 1;
        $category->increment('number_of_book', 1);
        $category->save();

        /*Thu muc cha chua thu muc con*/
        $parentCategory = Category::where('id', $category->parentId)->first();
        $parentCategory->increment('number_of_book', 1);
        $parentCategory->save();

        $book->category_name = $category->categoryname;

        /*Lay thong tin nguoi dang sach*/
        $nguoi_dang_bai = Auth::user();
        $book->nguoi_dang_bai = $nguoi_dang_bai->username;
        $book->id_nguoi_dang = $nguoi_dang_bai->id;

        $fileName = $request->file->getClientOriginalName();
        $request->file->move('upload', $fileName);
        $book->file_path = 'upload/' . $fileName;
        $book->save();

        return redirect()->route('book-show')
            ->with('addSuccess', 'You have successfully added new book');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::where('id', $id)->first();
        $categories = Category::whereNotNull('parentId')->get(); /*Tra ve thu muc con de select*/
        return view('book/view', ['book' => $book, 'categories' => $categories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**Xu Ly Rating */
    public function bookRate(Request $request)
    {
        request()->validate(['rate' => 'required']);
        $book = Book::find($request->id);
        $rating = new \willvincent\Rateable\Rating;
        $rating->rating = $request->rate;
        $rating->user_id = auth()->user()->id;
        $book->ratings()->save($rating);
        return redirect()->route("book-manage");
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $book = Book::where('id', $id)->first();
        $category = Category::where('id', $input['categoryid'])->first();

        $book->bookname = $input['bookname'];
        $book->author = $input['author'];
        $book->review_content = $input['review'];

        $fileName = $request->file->getClientOriginalName();
        $request->file->move('upload', $fileName);
        $book->file_path = 'upload/' . $fileName;

        /*Thu Muc Moi co Thu Muc Cha Khac Voi Thu Muc Cu && Thu Muc Moi Khac Thu Muc Cu*/
        if ($book->category->id != $input['categoryid'] && $book->category->parentId != $category->parentId) {
            /*Xu ly o thu muc con hien tai*/
            $book->category->decrement('number_of_book', 1);
            if ($book->category->number_of_book == 0) {
                $book->category->has_books = 0;
            }
            /*Xu ly o thu muc cha hien tai*/
            $parentCategory = Category::where('id', $book->category->parentId)->first();
            $parentCategory->decrement('number_of_book', 1);
            if ($parentCategory->number_of_book == 0) {
                $parentCategory->has_books = 0;
            }

            $parentCategory->save();
            $book->category->save();

            /*Thu muc moi nhan book*/
            $result = $category->increment('number_of_book', 1);
            $category->has_books = 1;
            /*Thu muc cha cua thu muc moi*/
            $newParentCategory = Category::where('id', $category->parentId)->first();
            $newParentCategory->increment('number_of_book', 1);
            $newParentCategory->has_books = 1;

            $newParentCategory->save();
            $category->save();
        } 
        /*Thu Muc Moi Co Thu Muc Cha Giong Voi Thu Muc Cu*/
        elseif ($book->category->parentId == $category->parentId) {
            /*Xu ly o thu muc con hien tai*/
            $book->category->decrement('number_of_book', 1);
            if ($book->category->number_of_book == 0) {
                $book->category->has_books = 0;
            }

            /*Thu muc moi nhan book*/
            $result = $category->increment('number_of_book', 1);
            $category->has_books = 1;

            $book->category->save();
            $category->save();
        }

        $book->category_name = $category->categoryname;
        $book->category_id = $input['categoryid'];
        $rating = new \willvincent\Rateable\Rating;
        $rating->rating = $request->rate;
        $rating->user_id = auth()->user()->id;
        $book->ratings()->save($rating);

        $book->save();

        return redirect()->route('book-description', $id)
            ->with('updateSuccess', 'You have successfully updated the book');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::where('id', $id)->first();

        /*Giam So Luong Sach tai Thu Muc Con*/
        $result = $book->category->decrement('number_of_book', 1);
        if ($book->category->number_of_book == 0) {
            $book->category->has_books = 0;
        }

        /*Giam So Luong Sach tai Thu Muc Cha */
        $parentCategory = Category::where('id', $book->category->parentId)->first();
        $parentCategory->decrement('number_of_book' ,1);
        if ($parentCategory->number_of_book == 0) {
            $parentCategory->has_books = 0;
        }

        $parentCategory->save();
        $book->category->save();

        /*Xoa Sach*/
        $destroy = Book::destroy($id);
        return redirect()->route('book-show')->with('deleteSuccess', 'You have deleted the book');
    }
}
