<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:20',
            'name' => 'required|max:20',
            'email' => 'required|max:50',
            'password' => 'required|max:20',
            'role' => '',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute Khong duoc de trong',
            'max' => ':attribute Khong duoc qua :max ky tu',
        ];
    }

    public function attributes(){
        return [
            'username'=>'Username',
            'name' => 'Ten',
            'email' => 'Email',
            'password' => 'Mat Khau',
        ];
    }
}
