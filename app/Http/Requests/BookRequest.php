<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bookname' => 'required|max:50',
            'author' => 'required|max:50',
            'review' => 'required',
            'file' => 'required|mimes:jpg,jpeg',
            'categoryid' => '',
        ];
    }

    /*Error Messages*/
    public function messages()
    {
        return [
            'required' => ':attribute khong duoc de trong',
            'max' => ':attribute khong duoc qua :max ky tu',
            'mimes' => 'Khong the Upload file kieu :attribute',
        ];
    }

    /*Attribute*/
    public function attributes(){
        return [
            'bookname'=> 'Ten sach',
            'author' => 'Ten tac gia',
            'review' => 'Danh gia',
            'file' => 'Tep',
        ];
    }
}
