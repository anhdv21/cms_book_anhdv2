<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    
    {
        return [
            'categoryname' => 'required|min:5|max:20',
            'parentcategoryid' => '',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute Khong duoc de trong',
            'max' => ':attribute Khong duoc qua :max ky tu',
            'min' => ':attribute Khong duoc it hon :min ky tu',
        ];
    }

    public function attributes(){
        return [
            'categoryname'=>'Ten Category',
        ];
    }
}
