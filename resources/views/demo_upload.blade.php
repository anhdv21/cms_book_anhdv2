@extends('adminlte::page')

@section('content')
@if(session()->has('success'))
    <div class="alert alert-info">
        {{ session()->get('success') }}
    </div>
@endif
<div class="card">
    <div class="card-header">Demo Upload</div>
    <div class="card-body">
    <form action="{{ route('file-upload') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
  
                <div class="col-md-6">
                    <input type="file" name="file" class="form-control">
                </div>
   
                <div class="col-md-6">
                    <button type="submit" class="btn btn-success">Upload</button>
                </div>
   
            </div>
        </form>
    </div>
</div>

@endsection