@extends('adminlte::page')

@section('content')
@if(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif

@if(session()->has('hasBookError'))
    <div class="alert alert-danger">
        {{ session()->get('hasBookError') }}
    </div>
@endif

@if(session()->has('deleteSuccess'))
    <div class="alert alert-info">
        {{ session()->get('deleteSuccess') }}
    </div>
@endif

@if(session()->has('success'))
    <div class="alert alert-info">
        {{ session()->get('success') }}
    </div>
@endif

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Category List<a class="btn btn-default float-right" href="{{ route('category-add-form') }}">Add Category</a>
            </div>
                <div class="card-body">
                    <ul data-widget="treeview">
                        @foreach( $categories as $category)

                            @if( $category->parentId == null)
                            <li class="treeview">
                                <a href="{{ route('category-description',$category->id) }}">{{ $category->categoryname }}({{ $category->number_of_book }})</a>
                                <ul class="treeview-menu">
                                    @foreach( $childCategories as $childCategory)

                                        @if( $childCategory->parentId == $category->id ) <!-- Thu Muc Hien Tai La Thu Muc Con Cua $category-->
                                            <li><a href="{{ route('category-description',$childCategory->id) }}">{{ $childCategory->categoryname }}({{ $childCategory->number_of_book }})</a></li>
                                        @endif

                                    @endforeach
                                </ul>
                            </li>
                            @endif

                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection