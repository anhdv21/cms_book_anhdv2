@extends('adminlte::page')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Add category') }}
                    <form action="{{ route('category-show') }}" method="POST" role="search" class="float-right">
                        {{ csrf_field() }}
                        <input name="search" placeholder="Search Category">
                        <button type="submit" class="btn btn-default">
                            Search
                        </button>
                    </form>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('category-add') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="categoryname" class="col-md-4 col-form-label text-md-right">{{ __('Category name') }}</label>

                            <div class="col-md-6">
                                <input id="categoryname" type="name" class="form-control @error('name') is-invalid @enderror" name="categoryname" value="{{ old('categoryname') }}" autocomplete="categoryname" autofocus>
                                @error('categoryname')
                                <small class="form-text text-muted">{{ $message }}</small>
                                @enderror

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('Parent Category') }}</label>

                            <div class="form-check">
                                <select class="form-control" name="parentcategoryid">
                                    <option value="none">None</option>
                                    @foreach( $categories as $category ) <!-- Select Thu Muc Cha -->
                                    <option value="{{$category->id}}">{{$category->categoryname}}</option>
                                    @endforeach
                                </select>
                            </div>
                            &nbsp;&nbsp;&nbsp;

                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Add Category') }}
                                </button>
                                <a class="btn btn-link" href="{{route('category-show')}}">Back</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection