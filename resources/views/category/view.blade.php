@extends('adminlte::page')

@section('content')
@if(session()->has('success'))
<div class="alert alert-info">
    {{ session()->get('success') }}
</div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Category Description') }}
                    <form action="{{ route('category-delete',$category->id) }}" method="POST" class="float-right">
                        {{ csrf_field() }}

                        <button type="submit" class="btn btn-primary">Delete</button>
                    </form>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('category-update',$category->id) }}">
                        @csrf

                        <div class="form-group row">
                            <label for="categoryname" class="col-md-4 col-form-label text-md-right">{{ __('Category name') }}</label>

                            <div class="col-md-6">
                                <input id="categoryname" type="name" class="form-control @error('name') is-invalid @enderror" name="categoryname" value="{{ $category->categoryname }}" autocomplete="categoryname" autofocus>
                                @error('categoryname')
                                <small class="form-text text-muted">{{ $message }}</small>
                                @enderror

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('Parent Category') }}</label>

                            <div class="form-check">
                                <select class="form-control" name="parentcategoryid">
                                    @if( $category->parentId == null)
                                    <option value="none" selected>None</option>
                                    @endif
                                    @foreach( $categories as $categorySelect )
                                    @if($categorySelect->id == $category->parentId)
                                    <option value="{{$categorySelect->id}}" selected>{{$categorySelect->categoryname}}</option>
                                    @else
                                    <option value="{{$categorySelect->id}}">{{$categorySelect->categoryname}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                            &nbsp;&nbsp;&nbsp;

                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __(' Update') }}
                                </button>
                                <a class="btn btn-link" href="{{route('category-show')}}">Back</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-header">{{ __('Books In Category') }}
                </div>
                <div class="card-body">
                <div class="card-body">
                    <div class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover dataTable" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting" rowspan="1" colspan="1">Id</th>
                                            <th class="sorting" rowspan="1" colspan="1">Name</th>
                                            <th class="sorting" rowspan="1" colspan="1">Author</th>
                                            <th class="sorting" rowspan="1" colspan="1">Category</th>
                                            <th class="sorting" rowspan="1" colspan="1">Review</th>
                                            <th class="sorting" rowspan="1" colspan="1">Nguoi Dang Sach</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($books as $book)
                                        <tr role="row" class="odd">
                                            <td>{{ $book->id }}</td>
                                            <td>{{ $book->bookname }}</td>
                                            <td>{{ $book->author }}</td>
                                            <td>{{ $book->category_name }}</td>
                                            <td>{{ $book->review_content }}</td>
                                            <td>{{ $book->nguoi_dang_bai }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection