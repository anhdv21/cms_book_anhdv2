@extends('adminlte::page')

@section('content_header')

@endsection

@section('content')
<div class="card-header">User Manage</div>    
<div class="card-body">
    
    <table class="table table-bordered" id="data-table">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th width="100px">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

</div>
   
@endsection
 
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
   $(document).ready( function () {
    
    $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('users-list') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    
  });
</script>
@endsection