@extends('adminlte::page')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div>
                        Welcome {{$name}}.
                        You are currently logged in as
                        @if( $level === 1)
                        {{ "Admin" }}
                        @elseif( $level === 2)
                        {{ "CTV" }}
                        @endif

                        @if($level === 1)
                        <div class="card-body">
                            Please choose one option below<br><br>
                            <a class="btn btn-primary" href="{{ route('account-show') }}">Manage account</a><br><br>
                            <a class="btn btn-primary" href="{{ route('category-show') }}">Manage categories</a><br><br>
                            <a class="btn btn-primary" href="{{ route('book-show') }}">Manage books</a>
                        </div>
                        @endif

                        @if($level === 2)
                        <div class="card-body">
                            <a class="btn btn-primary" href="{{ route('book-show') }}">Manage your books</a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection