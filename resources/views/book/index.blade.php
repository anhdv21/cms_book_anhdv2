@extends('adminlte::page')

@section('content')

@if(session()->has('addSuccess'))
<div class="alert alert-info">
    {{ session()->get('addSuccess') }}
</div>
@endif

@if(session()->has('deleteSuccess'))
<div class="alert alert-info">
    {{ session()->get('deleteSuccess') }}
</div>
@endif

<div class="card-header">Book List<a class="btn btn-default float-right" href="{{ route('book-add-form') }}">Add Book</a></div>
<div class="card-body" style="width:100%">

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style="width:1000px">
                    <div class="card-header">{{ __('Book List') }}<a class="btn btn-default float-right" href="{{ route('book-add-form') }}">Add Book</a>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-bordered table-hover" id="dataTable" role="grid">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting" rowspan="1" colspan="1">Id</th>
                                                <th class="sorting" rowspan="1" colspan="1">Thumbnail</th>
                                                <th class="sorting" rowspan="1" colspan="1">Name</th>
                                                <th class="sorting" rowspan="1" colspan="1">Author</th>
                                                <th class="sorting" rowspan="1" colspan="1">Category</th>
                                                <th class="sorting" rowspan="1" colspan="1">Review</th>
                                                <th class="sorting" rowspan="1" colspan="1">Nguoi Dang Sach</th>
                                                <th class="sorting" rowspan="1" colspan="1">Rating</th>
                                                <th class="sorting" rowspan="1" colspan="1">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($books as $book)
                                            <tr role="row" class="odd">
                                                <td>{{ $book->id }}</td>
                                                <td><img src="../public/{{ $book->file_path }}" width="50" height="50"></td>
                                                <td>{{ $book->bookname }}</td>
                                                <td>{{ $book->author }}</td>
                                                <td>{{ $book->category_name }}</td>
                                                <td>{{ $book->review_content }}</td>
                                                <td>{{ $book->nguoi_dang_bai }}</td>
                                               <td><input id="input-1" name="input-1" class="rating rating-loading" data-min="0" data-max="5" data-step="0.1" value="{{ $book->averageRating }}" data-size="xs" disabled="" style="width:100%"></td>
                                                <td>
                                                    @if( $book->id_nguoi_dang == $user->id)
                                                    <!-- Sach Cua User Dang Len -->
                                                    <form action="{{ route('book-description', $book->id) }}" method="POST">
                                                        {{ csrf_field() }}

                                                        <button class="btn btn-info">View</button>
                                                    </form>&nbsp;
                                                    <form action="{{ route('book-delete', $book->id) }}" method="POST">
                                                        {{ csrf_field() }}

                                                        <button class="btn btn-danger">Delete</button>
                                                    </form>
                                                    @else
                                                    <!-- Sach Cua User Khac Dang Len -->
                                                    @if( $user->level == 1)
                                                    <!-- Neu User La Admin -->
                                                    <form action="{{ route('book-description', $book->id) }}" method="POST">
                                                        {{ csrf_field() }}

                                                        <button class="btn btn-info">View</button>
                                                    </form>&nbsp;
                                                    <form action="{{ route('book-delete', $book->id) }}" method="POST">
                                                        {{ csrf_field() }}

                                                        <button class="btn btn-danger">Delete</button>
                                                    </form>
                                                    @endif
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-link" href="{{url('/home')}}">Back</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet"> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>

<script>
        $(document).ready(function() {
        var table = $('#dataTable').DataTable({
            orderCellsTop: true,
            ordering: true,
            order: [7, 'asc'],
            fixedHeader: true,
            paging: true,
            dom: 'Bfrtip',
            buttons: [{extend: 'csvHtml5',text: 'CSV',filename:'myExport',exportOptions: {
                    columns: '2,3,4'
                }},
                {extend: 'excelHtml5',text: 'Excel',filename:'myExport',exportOptions: {
                    columns: '2,3,4'
                }}],
        });

        $('#dataTable thead tr').clone(true).appendTo('#dataTable thead');
        $('#dataTable thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            if (title.localeCompare("Thumbnail") != 0 && title.localeCompare("Action") != 0 && title.localeCompare("Rating") != 0) { /*Neu cot Khac Thumbnail hoac Action*/
                $(this).html('<input type="text" placeholder="Search" style="width: 100%;" />');
            } else {
                $(this).html(''); /*Neu Cot La Thumbnail hoac Action Thi De Trong*/
            }

            $('input', this).on('keyup change', function() {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            })
        });

        
    });
</script>
@endsection
