@extends('adminlte::page')

@section('content')

@if(session()->has('updateSuccess'))
<div class="alert alert-info">
    {{ session()->get('updateSuccess') }}
</div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Book Description') }}
                    <form action="{{ url('/') }}" method="POST" role="search" class="float-right">
                        {{ csrf_field() }}
                        <input name="search" placeholder="Search Category">
                        <button type="submit" class="btn btn-default">
                            Search
                        </button>
                    </form>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('book-update',$book->id) }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="bookname" class="col-md-4 col-form-label text-md-right">{{ __('Book name') }}</label>

                            <div class="col-md-6">
                                <input id="bookname" type="name" class="form-control @error('bookname') is-invalid @enderror" name="bookname" value="{{ $book->bookname }}" autocomplete="bookname" autofocus>
                                @error('bookname')
                                <small class="form-text text-muted">{{ $message }}</small>
                                @enderror

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="author" class="col-md-4 col-form-label text-md-right">{{ __('Author') }}</label>

                            <div class="col-md-6">
                                <input id="author" type="name" class="form-control @error('author') is-invalid @enderror" name="author" value="{{ $book->author }}" autocomplete="author" autofocus>
                                @error('author')
                                <small class="form-text text-muted">{{ $message }}</small>
                                @enderror

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="category" class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>

                            <div class="form-check">
                                <select class="form-control" name="categoryid">
                                    @foreach( $categories as $category )
                                    <!-- Thu Muc Con De Select -->
                                    @if( $category->id == $book->category_id )
                                    <option value="{{$category->id}}" selected>{{$category->categoryname}}</option>
                                    @else
                                    <option value="{{$category->id}}">{{$category->categoryname}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                            &nbsp;&nbsp;&nbsp;

                        </div>

                        <div class="form-group row">
                            <label for="review" class="col-md-4 col-form-label text-md-right">{{ __('Review') }}</label>

                            <div class="col-md-6">
                                <textarea id="review" name="review">{{$book->review_content}} </textarea>
                                <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
                                <script>
                                    CKEDITOR.replace('review');
                                </script>
                                @error('review')
                                <small class="form-text text-muted">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row">
                        <label for="review" class="col-md-4 col-form-label text-md-right">{{ __('Rating') }}</label>
                        <input id="input-1" name="rate" class="rating rating-loading" data-min="0" data-max="5" data-step="1" value="{{ $book->userAverageRating }}" data-size="xs">
                        <input type="hidden" name="id" required="" value="{{ $book->id }}">
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <input type="file" name="file" class="form-control" id="fileUpload"><br>
                                <div id="image-holder"><img src="../{{$book->file_path}}" width="100" height="100"></div>
                        </div>
                        </div>

                        
                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __(' Update') }}
                        </button>
                        <a class="btn btn-link" href="{{route('book-show')}}">Back</a>
                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>
</div>
</div>

@endsection

@section('js')
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet"> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>

<script>
    $("#fileUpload").on('change', function () {

var imgPath = $(this)[0].value;
var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();

if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
    if (typeof (FileReader) != "undefined") {

        var image_holder = $("#image-holder");
        image_holder.empty();

        var reader = new FileReader();
        reader.onload = function (e) {
            $("<img />", {
                "src": e.target.result,
                    "class": "thumb-image",
                    "width": "100",
                    "height": "100"
            }).appendTo(image_holder);

        }
        image_holder.show();
        reader.readAsDataURL($(this)[0].files[0]);
    } else {
        alert("This browser does not support FileReader.");
    }
} else {
    alert("Pls select only images");
}
});


</script>
@endsection