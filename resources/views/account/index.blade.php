@extends('adminlte::page')

@section('content')
@if(session()->has('success'))
    <div class="alert alert-info">
        {{ session()->get('success') }}
    </div>
@endif


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Account List') }}<a class="btn btn-default float-right" href="{{ route('account-add-form') }}">Add Account</a>
                </div>
                <div class="card-body">
                    <div class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover dataTable" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting" rowspan="1" colspan="1">Id</th>
                                            <th class="sorting" rowspan="1" colspan="1">User Name</th>
                                            <th class="sorting" rowspan="1" colspan="1">Name</th>
                                            <th class="sorting" rowspan="1" colspan="1">Email</th>
                                            <th class="sorting" rowspan="1" colspan="1">Role</th>
                                            <th class="sorting" rowspan="1" colspan="1">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($accounts as $account)
                                        <tr role="row" class="odd">
                                            <td>{{ $account->id }}</td>
                                            <td>{{ $account->username }}</td>
                                            <td>{{ $account->name }}</td>
                                            <td>{{ $account->email }}</td>
                                            <td>
                                                @if( $account->level == 1)
                                                {{ "Admin" }}
                                                @else
                                                {{ "CTV" }}
                                                @endif
                                            </td>
                                            <td>
                                                <form action="{{ route('account-description',$account->id) }}" method="POST">
                                                    {{ csrf_field() }}

                                                    <button>View</button>
                                                </form><br>
                                                <a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="{{ route( 'account-delete',$account->id ) }}"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $accounts->links() }}
                            </div>
                        </div>
                    </div>
                    <a class="btn btn-link" href="{{url('/home')}}">Back</a>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection