<?php


use Illuminate\Support\Facades\Route;
use App\User;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Request;
use App\Model\Category;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('/home', 'HomeController@index')->name('home');

Route::get('/login-register',function(){
    return view ('auth/register');
});

Route::get('/', function () {
    return view('welcome');
});
*/
/*Route for authentication*/


/*Route for category manage*/
Route::any('/category-manage','CategoryController@index')->name('category-show');

Route::any('/category-add-form', 'CategoryController@create')->name('category-add-form');
Route::any('/add-category', 'CategoryController@store')->name('category-add');

Route::any('/category-description/{id}', ['uses'=>'CategoryController@show'])->name('category-description');

Route::any('/category-update/{id}', ['uses'=>'CategoryController@update'])->name('category-update');

Route::any('/category-delete/{id}', ['uses'=>'CategoryController@destroy'])->name('category-delete');

/*Route for account managing*/
Route::any('/account-manage','AccountController@index')->name('account-show');

Route::any('/account-description/{id}', ['uses'=>'AccountController@show'])->name('account-description');

Route::any('/account-add-form', 'AccountController@create')->name('account-add-form');
Route::any('/add-account', 'AccountController@store')->name('account-add');

Route::any('/account-update/{id}', ['uses'=>'AccountController@update'])->name('account-update');

Route::any('/account-delete/{id}', 'AccountController@destroy')->name('account-delete');

/*Route for authentication*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*Route for book managing */
Route::any('/book-add-form', 'BookController@create')->name('book-add-form');
Route::any('/add-book', 'BookController@store')->name('book-add');

Route::any('/book-manage',['uses'=>'BookController@index'])->name('book-show');

Route::any('/book-description/{id}', ['uses' => 'BookController@show'])->name('book-description');
Route::any('/book-rating','BookController@bookRate')->name('book-rating');

Route::any('/book-update/{id}', ['uses'=>'BookController@update'])->name('book-update');

Route::any('/book-delete/{id}', ['uses'=>'BookController@destroy'])->name('book-delete');

/*Test*/
